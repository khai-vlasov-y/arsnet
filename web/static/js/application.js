$(document).ready(function(){
    //connect to the socket server.
    let socket = io.connect('http://' + document.domain + ':' + location.port + '/test');
    let numbers_received = [];

    //receive details from server
    socket.on('newnumber', function(msg) {
        $('#sensors').empty();
        $('#sensors_data').empty();

        msg.sensors.forEach(function(row) {
            $('#sensors').append(`<tr><td>${row.id}</td><td>${row.description}</td><td>${row.last_sent}</td></tr>`);
        });
        msg.sensors_data.sort(function (a, b) {
            let comparison = 0;
            let data1 = JSON.parse(a.formatted);
            let data2 = JSON.parse(b.formatted);

            if (data1.recieved_at > data2.recieved_at) {
                comparison = 1;
            } else if (data2.recieved_at > data1.recieved_at) {
                comparison = -1;
            }

            return comparison;
        });
        msg.sensors_data.reverse().forEach(function(row) {
            let data = JSON.parse(row.formatted);
            let data_correct = data.is_correct?"" :" class='table-warning'";
            $('#sensors_data').append(`<tr ${data_correct}>
                <td>${row.fk_arduino_id}</td>
                <td>${data.type}</td>
                <td>${data.data}</td>
                <td>${row.recieved_at}</td>
                </tr>`);
        });
    });
});