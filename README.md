# Brief description

Server is written on Python 2 using Flask microframework in April 2018 by Dmitry Terenik.

# Example of launching arsnet-web:

```bash
DB_HOST=localhost
DB_PORT=5432
DB_USER=arsnet
DB_PASSWORD=arsnet
DB_NAME=arsnet
DATABASE_CONNECTION_STRING=postgresql://$DB_USER:$DB_PASSWORD@$DB_HOST:DB_PORT/$DB_NAME

# building containers
echo '[INFO] Building arsnet-web image...'
docker build -t arsnet-web . >/dev/null
echo '[INFO] Done!'
# starting server
echo '[INFO] Starting arsnet-web server.'
docker run -d --rm \
	--name arsnet-web \
	-p 80:5000 \
	-e APP_SETTINGS="config.DevelopmentConfig" \
    -e DATABASE_URL=$DATABASE_CONNECTION_STRING \
	arsnet-web >/dev/null
echo '[INFO] Done!'
```

# Access port

By default, server listen 5000 port.

# Environment variables

## Server settings:
| Var name | Required | Default value | Description |
| - | - | - | - | 
| DATABASE_URL | Yes | - | database connection string |
| APP_SETTINGS | Yes | - | path to config (should be 'config.DevelopmentConfig') |