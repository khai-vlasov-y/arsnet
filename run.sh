#!/usr/bin/env sh

set -e

DB_HOST=localhost
DB_PORT=5432
DB_USER=arsnet
DB_PASSWORD=arsnet
DB_NAME=arsnet
DATABASE_CONNECTION_STRING=postgresql://$DB_USER:$DB_PASSWORD@$DB_HOST:DB_PORT/$DB_NAME

# building containers
echo '[INFO] Building arsnet-web image...'
docker build -t arsnet-web . >/dev/null
echo '[INFO] Done!'
# starting server
echo '[INFO] Starting arsnet-web server.'
docker run -d --rm \
	--name arsnet-web \
	-p 80:5000 \
	-e APP_SETTINGS="config.DevelopmentConfig" \
    -e DATABASE_URL=$DATABASE_CONNECTION_STRING \
	arsnet-web >/dev/null
echo '[INFO] Done!'
# done!